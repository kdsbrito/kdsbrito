<?php require_once "src/Array_Helper.php"; ?>
<html>
	<head>
		<link href="css/styles.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
	<div class="container">
		<h1>BandsRank</h1>
		<span class="link">
			<a href="index.php">Voltar</a>
		</span>
		<table>
			<thead>
				<tr>
					<th>Banda</th>
					<th>Votos</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$rank_array = Array_Helper::get_rank();
				foreach($rank_array as $rank)
				{
					$banda = $rank['banda'];
					$votos = $rank['votos'];
					echo "<tr>
						<td>$banda</td>
						<td>$votos</td>
					</tr>";	
				}
				?>
			</tbody>
			
		</table>	
	</div>
</body>
</html>