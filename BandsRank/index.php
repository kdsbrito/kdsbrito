<?php require_once "src/Array_Helper.php"; ?>
<html>
	<head>
		<link href="css/styles.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
	<div class="container">
		<h1>BandsRank</h1>
		<span class="link">
			<a href="rank.php">Ver Votos</a>
		</span>
		<table>
			<thead>
				<tr>
					<th>M&uacute;sica</th>
					<th>Banda</th>
					<th>Ouvinte</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$rank_array = Array_Helper::get_list();
				foreach($rank_array as $rank)
				{
					echo "<tr>
						<td>$rank[0]</td>
						<td>$rank[1]</td>
						<td>$rank[2]</td>
					</tr>";	
				}
				?>
			</tbody>
			
		</table>	
	</div>
</body>
</html>