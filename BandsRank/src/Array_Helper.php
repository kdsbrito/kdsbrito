<?php
class Array_Helper{

	public static function get_list()
	{
		$file = "rank/data.csv";
		$objeto = fopen($file, 'r');
		$r = array();
		$i = 0;
		while(($dados = fgetcsv($objeto, 1000, ';')) != FALSE)
		{
			if($dados[1] != 'Banda')
			{
				$r[$i] = $dados;
				$i++;	
			}
		}
		fclose($objeto);
		return $r;
	}
	
	public static function remove_duplicate_values($array)
	{
		// Array Temporário
		$unique_array = array();
		
		foreach ($array as &$arr) {
	       if (!isset($unique_array[$arr[$key]]))
			{
				$unique_array[$arr[$key]] =& $arr;
			}
		}
		
		return array_values($unique_array);
	}
	
	public static function get_rank()
	{
		$list = self::get_list();
		$rank = array();
		
		for($i = 0; $i < count($list); $i++)
		{
			$rank[$i] = array('banda' => $list[$i][1], 'votos' => 1);
		}
		
		$votos = self::get_votos($rank);
		
		for($i = 0; $i < count($list); $i++)
		{
			$rank[$i] = array('banda' => $rank[$i]['banda'], 'votos' => $votos[$rank[$i]['banda']]);
		}
		
		$rank = array_unique($rank, SORT_DESC);
		foreach ($rank as $key => $row) {
		    $banda[$key]  = $row['banda'];
		    $votos[$key]  = $row['votos'];
		}
		
		usort($rank, 'Self::ordenar_rank');
		return $rank;
	}
	
	public static function ordenar_rank($item1, $item2)
	{
		return $item1['votos'] < $item2['votos'];
	}
	
	
	private static function get_votos($array)
	{
		$votos_array = array();
		for($i = 0; $i < count($array); $i++)
		{
			$votos_array[$i] = $array[$i]['banda'];
		}
		
		return array_count_values($votos_array);
	}
}
