<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="loginForm">
	<h1 class="title">Área Restrita</h1>

	<form method="post" action="<?php echo base_url('login'); ?>">
		<fieldset>
			<div class="errors">
				<?php if(isset($acess_error)): echo $acess_error; endif; ?>
			</div>
			<div>
				<input type="text" name="email" id="email" placeholder="E-mail"  />
				<?php echo form_error('email'); ?>
			</div>
			<div>
				<input type="password" name="pass" id="pass" placeholder="Senha" />
				<?php echo form_error('pass'); ?>
			</div>
			<div>
				<input type="submit"/>
			</div>
		</fieldset>
	</form>
</div>