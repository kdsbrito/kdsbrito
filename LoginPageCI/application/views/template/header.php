<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Sistema de Login com CodeIgniter</title>
		<meta name="author" content="Kleiton">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
		<link href="<?php echo base_url('assets/css/styles.css');?>" type="text/css" rel="stylesheet" />
	</head>

	<body>

	
