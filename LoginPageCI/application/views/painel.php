<div class="painel">

	<section>
		
		<h1>Sistema de Login com CodeIgniter</h1>
		<div class="sign-info">
			<p>
			Logado como: <?php echo $this->session->userdata('name'); ?>|
			<a href="<?php echo base_url('login/logout'); ?>">LogOut</a>
			</p>
		</div>
	</section>
	
	<section class="menu">
		<ul>
			<li><a href="<?php echo base_url('painel/paginaA'); ?>">Página A</a></li>
			<li><a href="<?php echo base_url('painel/paginaB'); ?>">Página B</a></li>
		</ul>
	</section>
</div>
