<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __contruct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$data = array('view' => 'login');
		$this->load->view('template', $data);
	}
}
