<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library("session");
	}
	
	public function index()
	{
		
		$this->verifica_sessao();
		$data = array('view' => 'painel');
		$this->load->view('template', $data);			
	}
	
	public function paginaA()
	{
		$this->verifica_sessao();
		$data = array('view' => 'paginaA');
		$this->load->view('template', $data);		
	}
	
	public function paginaB()
	{
		$this->verifica_sessao();
		$data = array('view' => 'paginaB');
		$this->load->view('template', $data);		
	}
	
	private function verifica_sessao()
	{
		if(empty($this->session->userdata("name")))
		{
			redirect(base_url());
		}
	}

	
}
