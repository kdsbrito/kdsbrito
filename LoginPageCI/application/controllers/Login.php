<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		if($this->validate_form()):
			$email = $this->input->post("email");
			$pass = $this->input->post("pass");
			if($this->doLogin($email, $pass)):
				redirect(base_url('painel'));
			else:
				$data = array('view' => 'login', 'acess_error' => "Usuário e/ou senha inválidos.");
				$this->load->view("template", $data);
			endif;
		else:
			$data = array('view' => 'login');
			$this->load->view("template", $data);
		endif;
			
	
	}
	
	
	
	private function doLogin($email, $pass)
	{
		$file = file(base_url('assets/db/users.csv'));
	
		foreach($file as $line):
			list($mail, $password, $active, $name) = explode(";", $line);
			if($mail == $email && $password == $pass && $mail != "email"):
				
				$session_data = array(
										'email'		=> $mail,
										'active' 	=> $active,
										'name'		=> $name
									 );
				$this->session->set_userdata($session_data);				
				return true;
			endif;
		endforeach;
		return false;
	}
	
	public function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('active');
		$this->session->unset_userdata('name');
		redirect(base_url());
	}
	
	
	private function validate_form()
	{		
		$this->form_validation->set_rules("email", "E-mail", "required");
		$this->form_validation->set_rules("pass", "Senha", "required");
		$this->form_validation->set_error_delimiters('<div class="errors">', '</div>');
		
		if($this->form_validation->run() == false):
			return false;
		
		else:
			return true;
				
		endif;
	}
}
